---
marp: true
theme: academic
paginate: true
math: katex
---

<!-- _class: lead -->

# Introdução sobre observabilidade

#### em sistemas distribuídos

**Aluno: Reinan Gabriel Dos Santos Souza**
Bacharelado em Sistemas de Informação
Instituto Federal de Sergipe
Campus Lagarto

<!-- _footer: '19 de outubro de 2023' -->

---

![bg left:40% 80%](https://avatars.githubusercontent.com/u/28494067?v=4)

**Reinan Gabriel Dos Santos Souza**

Sou um entusiasta de tecnologia apaixonado por aprender e explorar novas áreas.

Desenvolvedor desde 2018
Engenheiro DevOps Pleno – **MOVA**

https://linktr.ee/reinanhs

![w:128 h:128](https://www.qrtag.net/api/qr_1280.png?url=https://linktr.ee/reinanhs)

---

![bg left:40% 80%](https://www.qrtag.net/api/qr_1280.png?url=https://gitlab.com/snct-lagarto-2023)

**Arquivos para a apresentação**

Todos os materiais relacionados à apresentação do minicurso estão disponíveis digitalmente no meu repositório do **Gitlab**. Para acessar esses recursos, basta escanear o **QR Code** na imagem ao lado.

> Gitlab: https://gitlab.com/snct-lagarto-2023
> Github: https://github.com/ReinanHS

---

<!-- _header: Introdução -->

À medida que os sistemas distribuídos se tornaram mais populares, os desenvolvedores enfrentam um desafio crescente:

- Como solucionar problemas de software, considerando que ele pode ser composto por diversos serviços distintos, cada um executado em diferentes linguagens e plataformas?
- Como detectar mudanças críticas que possam impactar todos os nossos microsserviços?
- Como identificar as raízes reais dos erros?

---

Não muito tempo atrás, depurar um programa geralmente significava uma coisa: **Navegar nos logs de erros**

![w:800 center](./imagens/slack-laravel-logs.png)

---

Mas as coisas em TI estão sempre mudando. Os paradigmas de arquitetura de software evoluíram de monólitos para microsserviços.

![w:800 center](./imagens/monolith-microservices.jpg)

---

Com o surgimento do **DevOps**, os desenvolvedores agora têm novas responsabilidades. Isso significa que eles cuidam dos programas mesmo depois de serem entregues.

![w:690 center](./imagens/fluxo-de-devops.png)

---

# Por que a observabilidade é necessária?


A observabilidade é necessária para monitorar, detectar e resolver problemas em sistemas complexos, garantindo sua confiabilidade e desempenho.

---

<!-- _header: Importância da observabilidade para o sucesso empresarial -->

A **rapidez**, a **confiabilidade** e o **funcionamento** contínuo do software afetam o desempenho da empresa, o que inclui o **lucro**, o trabalho eficiente e a felicidade dos clientes. É essencial garantir que todas as solicitações de aplicativos sejam processadas de forma rápida e precisa.

![w:700 center](./imagens/profit-observability.png)

---

# Por que o mundo precisa de observabilidade?

A observabilidade é ainda mais importante agora para as empresas por algumas razões:

- Sistemas inteligentes em ascensão
- Demanda por acesso aos dados
- Requisitos de conformidade e precaução

---

<!-- _header: A tendência em alta no mercado atual -->

Observabilidade está se destacando como uma abordagem amplamente adotada e valorizada no mercado atual.

![](./imagens/observabilidade-trends.png)

> Disponível em: https://trends.google.com.br/trends/explore. Acesso em: 19 out. 2023.

---

<!-- _header: Complexidade de entender os serviços -->

A observabilidade é uma metodologia essencial para lidar com a **complexidade** dos **microsserviços**, que se tornaram uma tendência dominante no desenvolvimento de software.

![h:400 center](./imagens/distributed-tracing.jpg)

> Fonte da imagem: https://www.infoq.com/presentations/uber-microservices-distributed-tracing/

---


Para **Leo Cavalcante** (2022): “Observabilidade é a metodologia que o mercado de microsserviços e cloud-native explora para monitorar as aplicações”

> Leo Cavalcante. Observabilidade com Hyperf e OpenTelemetry. Medium, 22, mar, 2022. Disponível em: https://medium.com/inside-picpay/observabilidade-com-hyperf-e-opentelemetry-dd698844eda. Acesso em: 18 maio. 2023.

---

<!-- _header: A origem da observabilidade -->

O termo “**observabilidade**” teve sua origem no trabalho do engenheiro americano Rudolf E. Kálmán em agosto de 1960. conforme mencionado em seu livro “**Teoria dos sistemas de controle**” (On the general theory of control systems).

![center](./imagens/Rudolf_Kalman.jpg)

> Link para o livro: https://www.sciencedirect.com/science/article/pii/S1474667017700948
> Fonte para a imagem: https://pt.wikipedia.org/wiki/Rudolf_Kalman

---

Para **Rudolf E. Kálmán** a observabilidade é uma medida de quão bem os estados internos de um sistema podem ser inferidos a partir do conhecimento de suas saídas externas.

![h:300 center](./imagens/teoria-de-controle-exemplo.png)

> Fonte para a imagem: https://pt.wikipedia.org/wiki/Teoria_de_controle

---

Em suma, **Rudolf E. Kálmán**, lançou as bases do conceito de observabilidade, estabelecendo sua importância na determinação do estado interno dos sistemas.

![h:450 center](./imagens/teoria-dos-sistemas-de-controle.PNG)

---

<!-- _header: Surgimento das primeiras equipes de observabilidade em 2013 -->

Em 2013, a mudança para sistemas distribuídos já estava acontecendo.

Nesse ano, o X (anteriormente conhecido como 'Twitter') formou uma nova equipe, chamada  '*equipe de observabilidade*'. 

O principal objetivo dessa equipe é coletar dados de telemetria de centenas de serviços do Twitter de forma centralizada e padronizada.

---

<!-- _header: Publicação sobre observabilidade no Twitter -->

No final de 2013, a equipe de observabilidade do Twitter compartilhou um post no blog, descrevendo seus primeiros resultados ao usar a observabilidade na empresa. Eles também incluíram uma imagem mostrando o rastreamento de seus sistemas distribuídos.

![w:262 center](https://www.qrtag.net/api/qr_1280.png?url=https://blog.twitter.com/engineering/en_us/a/2013/observability-at-twitter)

> Observability at Twitter: https://blog.twitter.com/engineering/en_us/a/2013/observability-at-twitter

---

![h:500 center](https://cdn.cms-twdigitalassets.com/content/dam/blog-twitter/archive/observability_attwitter95.thumb.1280.1280.png)

> Fonte da imagem: https://blog.twitter.com/engineering/en_us/a/2013/observability-at-twitter
> Distributed Systems Tracing with Zipkin: https://blog.twitter.com/engineering/en_us/a/2012/distributed-systems-tracing-with-zipkin
---

<!-- _header: Publicação sobre abordagem da observabilidade em 2016 -->

O Twitter desempenhou um grande papel em popularizar o conceito de observabilidade na computação.

Em março de 2016, a equipe de engenharia de observabilidade do Twitter compartilhou o post [Observability at Twitter: technical overview](https://blog.twitter.com/engineering/en_us/a/2016/observability-at-twitter-technical-overview-part-i) no blog explicando sua abordagem à observabilidade. 

![w:262 center](https://www.qrtag.net/api/qr_1280.png?url=https://blog.twitter.com/engineering/en_us/a/2016/observability-at-twitter-technical-overview-part-i)

--- 

<!-- _header: Observabilidade hoje -->

Hoje, a observabilidade é essencial para lidar com aplicativos complexos. Desenvolvedores, engenheiros de TI e equipes de DevOps não podem viver sem ela.

---

<!-- _header: Princípios da observabilidade -->

A observabilidade se fundamenta em quatro pilares-chave:

![center](./imagens/pilares-da-observabilidade.png)

---

<!-- _header: Métricas -->

As métricas fornecem medidas quantitativas do desempenho e do comportamento dos sistemas.

- Monitoramento de tendências
- Identificação de padrões
- Base para tomada de decisões
- Otimização e escalonamento

---

<!-- _header: Métricas -->

![w:980 center](./imagens/cpu-utilization.png)

> Fonte da imagem: https://www.keypup.io/blog/cloudtasker-monitor-your-cloud-tasks-jobs-on-gcp

---

<!-- _header: RED Metrics  -->

RED Metrics fornece aos desenvolvedores um modelo para instrumentar seus serviços e construir painéis de controle de forma consistente e repetível.

- **R**ate: O número de solicitações que o serviço está processando por segundo.
- **E**rror: O número de solicitações com falha por segundo.
- **D**uration: A quantidade de tempo que cada solicitação leve.

---

<!-- _header: Logs  -->

Os logs são registros detalhados das atividades e eventos que ocorrem em um sistema.

- Ferramenta de depuração
- Registro de eventos relevantes
- Sequenciamento de eventos
- Contexto e causa de problemas

---

<!-- _header: Ilustração de logs com uma organização deficiente  -->

```
18-08-23 12:34:56 - INFO - Dados ausentes
18-08-23 12:34:56 - Evento: INFO - Ocorreu algo inesperado no cadastro do produto.
18-08-23 12:34:56 - ERRO: 500 - Problema com o sistema. Algo deu errado.
```

- Não identifica quais dados ou a causa da ausência.
- A falta de clareza na descrição do evento torna difícil entender o que realmente aconteceu.
- A mensagem de erro é vaga e não fornece informações úteis sobre o problema.

---

<!-- _header: Dicas para melhorar a organização dos logs com pequenas configurações  -->


```php
'default' => [
    'handler' => [
        'class' => Monolog\Handler\StreamHandler::class,
        'constructor' => [
            'stream' => 'php://stdout',
            'messageType' => Monolog\Handler\ErrorLogHandler::OPERATING_SYSTEM,
            'level' => Monolog\Level::Info,
        ],
    ],
    'formatter' => [
        'class' => Monolog\Formatter\JsonFormatter::class,
        'constructor' => [
            'includeStacktraces' => true,
        ],
    ],
    'PsrLogMessageProcessor' => [
        'class' => Monolog\Processor\PsrLogMessageProcessor::class,
    ],
    'processors' => [
        new Monolog\Processor\GitProcessor(),
        new Monolog\Processor\MemoryUsageProcessor(),
        new Monolog\Processor\ProcessIdProcessor(),
        new Monolog\Processor\WebProcessor(),
    ],
]
```

---

<!-- _header: Demonstração da organização de log para requisições -->

![w:1200 center](./imagens/gcp-exemplo-log-1.png)

---

<!-- _header: Explorando a organização de logs em casos de erros de validação -->

![w:1200 center](./imagens/gcp-exemplo-log-2.png)

---

<!-- _header: Análise de logs com Stack Traces: Exemplos e Insights -->

![w:1200 center](./imagens/gcp-exemplo-log-3.png)

---

<!-- _header: Rastreamento -->

O rastreamento, também conhecido como tracing, é a capacidade de acompanhar o fluxo de uma transação ou uma solicitação através de vários componentes de um sistema distribuído.

- Visibilidade em sistemas distribuídos
- Identificação de gargalos e problemas
- Suporte à resolução de problemas

---

<!-- _header: Rastreamento distribuído -->

![w:1100 center](./imagens/traces-spans.webp)

> Fonte da imagem: https://www.dynatrace.com/news/blog/open-observability-distributed-tracing-and-observability/

---

<!-- _header: GCP Cloud Trace -->

![w:1200 center](./imagens/exemplo-trace.png)

> Fonte da imagem: https://www.wix.com/velo/reference/velo-package-readmes/gcp-cloud-trace-integration

---

<!-- _header: Contexto de rastreamento do B3 propagation -->

O B3 é uma especificação para o cabeçalho das requisições em nossa aplicação. Essa especificação define identificadores usados para agrupar uma operação em uma árvore de rastreamento.

![center](./imagens/b3-propagation-exemplo.webp)

> Documentação oficial: https://github.com/openzipkin/b3-propagation
> Exemplificação dos cabeçalhos do B3 — Fonte do autor

---

<!-- _header: Exemplo em uma requisição HTTP usando o B3 propagation  -->

![center](./imagens/http-exemplo-b3.webp)

> Exemplificação dos cabeçalhos de uma solicitação Http usando B3 — Fonte do autor

---

<!-- _header: Contexto de rastreamento do X-Cloud-Trace-Context -->

- Ele é usado pelo Cloud Trace na GCP para correlacionar chamadas entre serviços e fornecer visibilidade da latência e do desempenho de um aplicativo.


```
"X-Cloud-Trace-Context: TRACE_ID/SPAN_ID;o=TRACE_TRUE"
```

```shell
curl "http://www.example.com" \
--header "X-Cloud-Trace-Context:105445aa7843bc8bf206b12000100000/1;o=1"
```

> Fonte: https://cloud.google.com/trace/docs/setup?hl=pt-br

---

<!-- _header: Observabilidade versus monitoramento -->

O monitoramento tradicional não funciona bem em sistemas complexos, como microsserviços e sistemas distribuídos. Ele só pode rastrear o que já é conhecido. Por exemplo:

- Qual a taxa de requisições por segundo do meu aplicativo?
- Quanta memória o Redis está usando?
- Qual é a latência média de uma solicitação?

---

No entanto, no decorrer da manutenção de aplicações, muitas vezes precisamos ser capazes de responder a perguntas mais profundas como estas:

- Onde está o gargalo no sistema causando latência para um endpoint específico?
- O que mais mudou em meu sistema que pode estar causando esse erro?
- Qual serviço preciso atualizar para corrigir esse erro?

---

![center](./imagens/observabilidade-vs-monitoramento.png)

---

<!-- _header: Ferramentas de observabilidade -->

Selecionei algumas ferramentas para mostrar como a observabilidade é usada. Lembre-se de que existem outras opções disponíveis, mas escolhi as mais comuns. Veja abaixo os principais tópicos que vão ser falados:

- Principais ferramentas para o monitoramento e análise de métricas.
- Principais ferramentas para o rastreamento distribuído.
- Principais ferramentas para o gerenciamento e centralização de logs.

---

<!-- _header: Prometheus -->

O Prometheus é um sistema de monitoramento de **código aberto** amplamente utilizado que coleta e armazena métricas sobre sistemas e aplicativos. Ele fornece uma estrutura flexível para a coleta de dados de telemetria.

![center](./imagens/prometheus-logo.webp)

---

<!-- _header: Grafana -->

O Grafana é uma plataforma de **código aberto** amplamente usada para visualização e monitoramento de dados em tempo real. Ele permite que os usuários criem painéis interativos e gráficos.

![center w:1000](./imagens/grafana-exemplo.webp)

---

<!-- _header: Jaeger -->

O Jaeger é uma plataforma de **código aberto** para rastreamento distribuído, projetada para monitorar e solucionar problemas em sistemas complexos, como microsserviços. 

![center w:1000](./imagens/jaeger-exemplo.webp)

---

<!-- _header: Zipkin -->

Outra ferramenta importante é o Zipkin. O Zipkin é outra plataforma de rastreamento distribuído de **código aberto** que oferece recursos semelhantes ao Jaeger.

![center w:1000](./imagens/zipkin-exemplo.webp)

---

<!-- _header: Cloud Trace -->

O Cloud Trace é um sistema de rastreamento distribuído que coleta dados de latência dos aplicativos e os exibe no Console do Google Cloud.

- US$ 0,20/milhão por trace processado

![center](./imagens/cloud-trace-list.png)

---

<!-- _header: Graylog -->

O Graylog é uma plataforma de gerenciamento de registros e análise de dados de **código aberto** que oferece uma solução centralizada para coletar, armazenar, pesquisar e analisar registros de sistemas e aplicativos.

![center w:1000](./imagens/graylog-exemplo.png)

---

<!-- _header: Cloud Logging -->

Gerenciamento de registros em tempo real e totalmente gerenciado com armazenamento, pesquisa, análise e alertas em escala de exabytes.

|RECURSO                   | PREÇO        |
|--------------------------|--------------|
|Armazenamento do Logging  | US$ 0,50/GiB |
|Retenção de registros     | US$ 0,01/GiB |

![center w:300](./imagens/cloud-logging.webp)

> Fonte: https://cloud.google.com/logging

---

<!-- _header: OpenTelemetry -->

OpenTelemetry é um projeto de **código aberto** que fornece um conjunto de APIs e SDKs para coletar, processar e exportar dados de telemetria de aplicativos e infraestrutura.

![center w:900](./imagens/otel-collector-exemplo.png)

---

<!-- _header: Destaque de outras ferramentas que merecem menção -->

|Ferramenta                 |Classificação|Avaliações|
|---------------------------|-------------|----------|
| Dynatrace                 | 4.5         |1398      |
| New Relic                 | 4.5         |1358      |
| Datadog                   | 4.4         |660       |
| IBM Instana Observability | 4.2         |273       |
| Amazon CloudWatch         | 4.4         | 217      |
| Elastic Observability     | 4.5         | 159      |

> Fonte: https://gartner.com/reviews/market/application-performance-monitoring-and-observability

---

<!-- _header: Dica de uma excelente ferramenta para desenhar uma stack  -->

![center](./imagens/openapm-exemplo.png)

> Fonte: https://openapm.io/landscape

---

<!-- _header: Explorando o uso dessas ferramentas em um cenário de exemplo  -->

![w:960 center](./imagens/exemplo-opentelemetry-hyperf.png)

---

<!-- _header: POC Distributed Tracing PHP  -->

Agora, vamos explorar como essas ferramentas estão conectadas e como os dados estão fluindo entre elas em um nível de código. O primeiro passo para obter essas informações é acessar o repositório no **GitHub** listado abaixo:

![w:328 h:328 center](https://www.qrtag.net/api/qr_1280.png?url=https://github.com/ReinanHS/poc-distributed-tracing-ph)

> Fonte: https://github.com/ReinanHS/poc-distributed-tracing-php

---

<!-- _header: O que é service mesh?  -->

Uma service mesh é uma camada de infraestrutura que fornece funcionalidades para a comunicação entre serviços em uma arquitetura de microsserviços. Ela é responsável por gerenciar o tráfego entre os serviços, fornecendo:

- Gerenciamento de tráfego
- Segurança
- Observabilidade
- Resiliência

---

<!-- _header: O que é Istio?  -->

Istio é uma service mesh open source criada pelo **Google** e pela **IBM**. É uma plataforma que permite aos desenvolvedores e equipes de operações gerenciar e proteger aplicativos baseados em microsserviços de forma eficiente.

- Alcance uma rede de serviços consistente.
- Proteja seus serviços com os benefícios do Istio.
- Melhore o desempenho do aplicativo.
- Alcance a observabilidade dos seus serviços.

> Fonte: https://cloud.google.com/learn/what-is-istio?hl=pt-br
---

<!-- _header: Observabilidade com o Istio -->

![w:1200 center](./imagens/kiale.png)

---

<!-- _header: Apresentação prática de observabilidade com o Istio -->

Para entender melhor como nossos microsserviços estão se comportando, vamos usar o Istio, uma ferramenta que fornece informações de observabilidade. Para começar, vamos acessar o repositório no **Gitlab** listado abaixo.

![w:328 h:328 center](https://www.qrtag.net/api/qr_1280.png?url=https://gitlab.com/snct-lagarto-2023/infrastructure-department/gcp-istio-poc)

> Fonte: https://gitlab.com/snct-lagarto-2023/infrastructure-department/gcp-istio-poc

---

<!-- _header: Conclusão -->

Espero que esta apresentação tenha fornecido uma visão inicial sobre os conceitos de observabilidade. 

- As tecnologias mencionadas ao longo desta publicação são importantes e amplamente utilizadas no mercado.
- Selecionei algumas tecnologias que considerei relevantes e que podem ser utilizadas localmente por meio do Docker.
- Lembre-se de que as ferramentas observabilidade são bastante poderosas para monitorar, depurar e otimizar seu projeto.
- A observabilidade é um tema amplo e em constante evolução.

---

<!-- _header: Perguntas e discussão -->

Agradeço a atenção de todos. Agora, vou separar um pequeno tempo para responder perguntas e dúvidas que possam ter surgido durante a minha explicação na apresentação.

