# Introdução sobre observabilidade em sistemas distribuídos

Este repositório tem a finalidade de armazenar os materiais da apresentação que abordará o tema da observabilidade em sistemas distribuídos.

## Software stack

Esse projeto roda nos seguintes softwares:

- Marp 2.7.0
- Docker

## Changelog

Consulte [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

## Contributors

Sinta-se à vontade para contribuir com melhorias, correções de bugs ou adicionar recursos a este repositório. Basta criar um fork do projeto, fazer suas alterações e enviar uma solicitação pull. Suas contribuições são bem-vindas!

Quer fazer parte deste projeto? leia [como contribuir](CONTRIBUTING.md).

## License

Este projeto está licenciado sob a licença MIT. Consulte o arquivo [LICENSE](LICENSE) para obter mais detalhes.

## Referências

- [Explorando o Hyperf: Um guia básico sobre observabilidade](https://medium.com/@reinanhs/explorando-o-hyperf-um-guia-b%C3%A1sico-sobre-observabilidade-1591febfb5c)
- [Observability for developers](https://www.ibm.com/downloads/cas/K6WDE918)
- [Observability Why & How?](https://www.ibm.com/nl-en/marketing/ibmlabs/Intro_into_Observability.pdf)
- [What is observability?](https://www.ibm.com/topics/observability#How+does+observability+work%3F)
- [O que é observabilidade?](https://newrelic.com/pt/blog/best-practices/what-is-observability#toc-observabilidade-versus-monitoramento)
- [Three Pillars of Observability](https://www.datadoghq.com/three-pillars-of-observability/)
- [Chapter 4. The Three Pillars of Observability](https://www.oreilly.com/library/view/distributed-systems-observability/9781492033431/ch04.html)
- [Observabilidade de dados: o que é, principais pilares e benefícios](https://blog.engdb.com.br/observabilidade-de-dados/)
- [Distributed Systems Tracing with Zipkin](https://blog.twitter.com/engineering/en_us/a/2012/distributed-systems-tracing-with-zipkin)
- [Observability at Twitter](https://blog.twitter.com/engineering/en_us/a/2013/observability-at-twitter)
- [Observability at Twitter: technical overview, part I](https://blog.twitter.com/engineering/en_us/a/2016/observability-at-twitter-technical-overview-part-i)
- [Learn the history — The Path to Observability](https://thetechnologist.in/learn-the-history-the-path-to-observability-a677f49fd4af)
- [From Kálmán to Kubernetes: A History of Observability in IT](https://academy.broadcom.com/blog/aiops/from-kalman-to-kubernetes-a-history-of-observability-in-it)
- [The Evolution of Observability](https://www.peakxv.com/article/the-evolution-of-observability/)
- [Observability: A Concept That Goes Back to the Founding of the Internet](https://cribl.io/blog/observability-history/)